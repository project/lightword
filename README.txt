WHAT IS Lightword?
------------------------

Lightword is a clean div based fixed width, 2 column layout theme with a an option to switch to a wider layout. 

FEATURES
--------

INSTALLATION
------------

 1. Download Lightword from http://drupal.org/project/lightword

 2. Unpack the downloaded files, take the folders and place them in your
    Drupal installation under one of the following locations:
      sites/all/themes
        making it available to the default Drupal site and to all Drupal sites
        in a multi-site configuration
      sites/default/themes
        making it available to only the default Drupal site
      sites/example.com/themes
        making it available to only the example.com site if there is a
        sites/example.com/settings.php configuration file

    Note: you will need to create the "themes" folder under "sites/all/"
    or "sites/default/".

FURTHER READING
---------------

Full documentation on using Lightword:
  http://www.freedrupalthemes.net/docs/lightword

Drupal theming documentation in the Theme Guide:
  http://drupal.org/theme-guide
  
Lightword demo site
  http://d7.freedrupalthemes.net/t/lightword
